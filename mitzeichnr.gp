set title 'Mitzeichneranzahl ePetition Vorratsdatenspeicherung'
set terminal png enhanced xFAFAFA size 800,600
set output 'Mitzeichneranzahl ePetition Vorratsdatenspeicherung.png'
set datafile separator ","

set xdata time
set style data fsteps
set xlabel "Uhrzeit"
set timefmt "%s"
set xtics 86400
set format x "%d.%m"

set ylabel "Mitzeichner"

set grid
set pointsize 2
set size 1,1

plot "mitzeichnr.csv" using ($1+7200):2 wi li
quit
