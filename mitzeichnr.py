import urllib2
import time
from BeautifulSoup import BeautifulSoup
import os

while True:
    # generate timestamp
    timestamp = str(round(time.time(), 0))[:-2]

    # load html
    with open("mitzeichnr.csv") as file:
        data = file.read()
    
    # parse 'anzahl'
    soup = BeautifulSoup(urllib2.urlopen('http://zeichnemit.de/').read())
    for row in soup('div', {'id' : 'anzahl'}):
        anzahl = row.text.split(' ')[0]

    # add new line to log
    data = data + '\n' + timestamp + ',' + anzahl

    # store
    with open("mitzeichnr.csv", 'w') as file:
        file.write(data)

    # render
    os.system('gnuplot mitzeichnr.gp')

    # sleep
    time.sleep(60)
